locals {
  ami_name = var.ami_name != null ? var.ami_name : "${var.name}-{{ imagebuilder:buildDate }}"
  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_cloudwatch_log_group" "ec2_image_builder" {
  name              = "/aws/imagebuilder/${var.name}"
  retention_in_days = var.log_retention
  tags              = local.tags
}

resource "aws_iam_role" "ec2_image_builder" {
  assume_role_policy = data.aws_iam_policy_document.ec2_assume.json
  name               = var.name
  tags               = local.tags
}

resource "aws_iam_instance_profile" "ec2_image_builder" {
  name = var.name
  role = aws_iam_role.ec2_image_builder.name
  tags = local.tags
}

data "aws_iam_policy_document" "ec2_assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ec2_instance_profile_for_image_builder" {
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilder"
  role       = aws_iam_role.ec2_image_builder.name
}

resource "aws_iam_role_policy_attachment" "ec2_instance_profile_for_image_builder_ecr_container_builds" {
  policy_arn = "arn:aws:iam::aws:policy/EC2InstanceProfileForImageBuilderECRContainerBuilds"
  role       = aws_iam_role.ec2_image_builder.name
}

resource "aws_iam_role_policy_attachment" "amazon_ssm_managed_instance_core" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
  role       = aws_iam_role.ec2_image_builder.name
}

resource "aws_security_group" "ec2_image_builder" {
  name   = var.name
  tags   = local.tags
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "ssh_ingress" {
  cidr_blocks       = var.ssh_ingress_cidr_blocks
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.ec2_image_builder.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "all_egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = -1
  protocol          = -1
  security_group_id = aws_security_group.ec2_image_builder.id
  to_port           = -1
  type              = "egress"
}

resource "aws_imagebuilder_infrastructure_configuration" "main" {
  instance_profile_name = aws_iam_instance_profile.ec2_image_builder.name
  name                  = var.name
  description           = var.description

  dynamic "instance_metadata_options" {
    for_each = var.instance_metadata_options[*] != null ? var.instance_metadata_options[*] : []

    content {
      http_put_response_hop_limit = can(instance_metadata_options.value.http_put_response_hop_limit) ? instance_metadata_options.value.http_put_response_hop_limit : null
      http_tokens                 = can(instance_metadata_options.value.http_tokens) ? instance_metadata_options.value.http_tokens : null
    }
  }

  instance_types = var.instance_types
  key_pair       = var.key_name

  dynamic "logging" {
    for_each = var.logging[*] != null ? var.logging[*] : []

    content {
      dynamic "s3_logs" {
        for_each = logging.value.s3_logs[*]

        content {
          s3_bucket_name = s3_logs.value.s3_bucket_name
          s3_key_prefix  = s3_logs.value.s3_key_prefix
        }
      }
    }
  }

  resource_tags                 = var.resource_tags
  security_group_ids            = [aws_security_group.ec2_image_builder.id]
  sns_topic_arn                 = var.sns_topic_arn
  subnet_id                     = var.subnet_id
  tags                          = local.tags
  terminate_instance_on_failure = var.terminate_instance_on_failure
}

resource "aws_imagebuilder_distribution_configuration" "main" {
  name = var.name

  distribution {
    region = data.aws_region.current.name

    ami_distribution_configuration {
      ami_tags           = var.ami_tags
      description        = var.ami_description
      kms_key_id         = var.kms_key_id
      name               = local.ami_name
      target_account_ids = var.target_account_ids

      dynamic "launch_permission" {
        for_each = var.launch_permission[*] != null ? var.launch_permission[*] : []

        content {
          organization_arns        = can(launch_permission.value.organization_arns) ? launch_permission.value.organization_arns : null
          organizational_unit_arns = can(launch_permission.value.organizational_unit_arns) ? launch_permission.value.organizational_unit_arns : null
          user_groups              = can(launch_permission.value.user_groups) ? launch_permission.value.user_groups : null
          user_ids                 = can(launch_permission.value.user_ids) ? launch_permission.value.user_ids : null
        }
      }
    }

    dynamic "launch_template_configuration" {
      for_each = var.launch_template_configuration[*] != null ? var.launch_template_configuration[*] : []

      content {
        default            = can(launch_template_configuration.value.default) ? launch_template_configuration.value.default : null
        account_id         = can(launch_template_configuration.value.account_id) ? launch_template_configuration.value.account_id : null
        launch_template_id = launch_template_configuration.value.launch_template_id
      }
    }

    license_configuration_arns = var.license_configuration_arns
  }

  description = var.description
  tags        = local.tags
}

resource "aws_imagebuilder_image_recipe" "main" {
  dynamic "component" {
    for_each = var.components

    content {
      component_arn = component.value.component_arn

      dynamic "parameter" {
        for_each = can(component.value.parameters) ? component.value.parameters : []

        content {
          name  = parameter.value.name
          value = parameter.value.value
        }
      }
    }
  }

  name         = var.name
  parent_image = var.parent_image
  version      = var.recipe_version

  dynamic "block_device_mapping" {
    for_each = var.block_device_mappings

    content {
      device_name = can(block_device_mapping.value.device_name) ? block_device_mapping.value.device_name : null

      dynamic "ebs" {
        for_each = can(block_device_mapping.value.ebs[*]) ? block_device_mapping.value.ebs[*] : []

        content {
          delete_on_termination = can(ebs.value.delete_on_termination) ? ebs.value.delete_on_termination : null
          encrypted             = can(ebs.value.encrypted) ? ebs.value.encrypted : null
          iops                  = can(ebs.value.iops) ? ebs.value.iops : null
          kms_key_id            = can(ebs.value.kms_key_id) ? ebs.value.kms_key_id : null
          snapshot_id           = can(ebs.value.snapshot_id) ? ebs.value.snapshot_id : null
          volume_size           = can(ebs.value.volume_size) ? ebs.value.volume_size : null
          volume_type           = can(ebs.value.volume_type) ? ebs.value.volume_type : null
        }
      }

      no_device    = can(block_device_mapping.value.no_device) ? block_device_mapping.value.no_device : null
      virtual_name = can(block_device_mapping.value.virtual_name) ? block_device_mapping.value.virtual_name : null
    }
  }

  description = var.description

  dynamic "systems_manager_agent" {
    for_each = var.systems_manager_agent[*] != null ? var.systems_manager_agent[*] : []

    content {
      uninstall_after_build = systems_manager_agent.value.uninstall_after_build
    }
  }

  user_data_base64  = var.user_data_base64
  working_directory = var.working_directory

  tags = local.tags
}

resource "aws_imagebuilder_image_pipeline" "main" {
  infrastructure_configuration_arn = aws_imagebuilder_infrastructure_configuration.main.arn
  name                             = var.name
  description                      = var.description
  distribution_configuration_arn   = aws_imagebuilder_distribution_configuration.main.arn
  enhanced_image_metadata_enabled  = var.enhanced_image_metadata_enabled
  image_recipe_arn                 = aws_imagebuilder_image_recipe.main.arn

  dynamic "image_tests_configuration" {
    for_each = var.image_tests_configuration[*] != null ? var.image_tests_configuration[*] : []

    content {
      image_tests_enabled = can(image_tests_configuration.value.image_tests_enabled) ? image_tests_configuration.value.image_tests_enabled : null
      timeout_minutes     = can(image_tests_configuration.value.timeout_minutes) ? image_tests_configuration.value.timeout_minutes : null
    }
  }

  dynamic "schedule" {
    for_each = var.schedule[*] != null ? var.schedule[*] : []

    content {
      schedule_expression                = schedule.value.schedule_expression
      pipeline_execution_start_condition = can(schedule.value.pipeline_execution_start_condition) ? schedule.value.pipeline_execution_start_condition : null
      timezone                           = can(schedule.value.timezone) ? schedule.value.timezone : null
    }
  }

  status = var.status
  tags   = local.tags
}
