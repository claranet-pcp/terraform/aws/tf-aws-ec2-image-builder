variable "ami_description" {
  type        = string
  description = "Description to apply to the distributed AMI"
  default     = null
}

variable "ami_name" {
  type        = string
  description = "Name of the created AMI"
  default     = null
}

variable "ami_tags" {
  type        = map(string)
  description = "Key-value map of tags to apply to the distributed AMI"
  default     = {}
}

variable "block_device_mappings" {
  type        = list(any)
  description = "Configuration block(s) with block device mappings for the image recipe"
  default     = []
}

variable "components" {
  type        = list(any)
  description = "Map of components for the recipie"
}

variable "description" {
  type        = string
  description = "Description to assign created resources"
  default     = null
}

variable "enhanced_image_metadata_enabled" {
  type        = bool
  description = "Whether additional information about the image being created is collected"
  default     = true
}

variable "image_tests_configuration" {
  type        = map(any)
  description = "Configuration block with image tests configuration"
  default     = null
}

variable "instance_metadata_options" {
  type        = map(any)
  description = "Configuration block with instance metadata options for the HTTP requests that pipeline builds use to launch EC2 build and test instances"
  default     = null
}

variable "instance_types" {
  type        = list(string)
  description = "Set of EC2 Instance Types to use for the image build"
  default     = ["t2.nano", "t3.micro"]
}

variable "key_name" {
  type        = string
  description = "Name of EC2 Key Pair for the build instance"
  default     = null
}

variable "kms_key_id" {
  type        = string
  description = "Amazon Resource Name (ARN) of the Key Management Service (KMS) Key to encrypt the distributed AMI"
  default     = null
}

variable "launch_permission" {
  type        = map(any)
  description = "Configuration block of EC2 launch permissions to apply to the distributed AMI"
  default     = null
}

variable "launch_template_configuration" {
  type        = map(any)
  description = "Set of launch template configuration settings that apply to image distribution"
  default     = null
}

variable "license_configuration_arns" {
  type        = list(string)
  description = "Set of Amazon Resource Names (ARNs) of License Manager License Configurations"
  default     = []
}

variable "logging" {
  type        = map(any)
  description = "Configuration block with logging settings"
  default     = null
}

variable "log_retention" {
  type        = string
  description = "CloudWatch logs retention in days"
  default     = 30
}

variable "name" {
  type        = string
  description = "Name to assign created resources"
}

variable "parent_image" {
  type        = string
  description = "Parent (source) AMI"
}

variable "recipe_version" {
  type        = string
  description = "Version of the image recipe"
  default     = "1.0.0"
}

variable "resource_tags" {
  type        = map(string)
  description = "Key-value map of resource tags to assign to infrastructure created by the configuration"
  default     = {}
}

variable "schedule" {
  type        = map(any)
  description = "Configuration block with schedule settings"
  default     = null
}

variable "sns_topic_arn" {
  type        = string
  description = "Amazon Resource Name (ARN) of SNS Topic for build notifications and alerts"
  default     = null
}

variable "ssh_ingress_cidr_blocks" {
  type        = list(string)
  description = "Set of CIDR blocks for SSH ingress"
  default     = ["0.0.0.0/0"]
}

variable "status" {
  type        = string
  description = "Status of the image pipeline"
  default     = "ENABLED"
}

variable "subnet_id" {
  type        = string
  description = "EC2 Subnet identifier for the build instance"
}

variable "systems_manager_agent" {
  type        = map(any)
  description = "Configuration block for the Systems Manager Agent installed by default by Image Builder"
  default     = null
}

variable "tags" {
  type        = map(string)
  description = "Key-value map of tags to apply to resources"
  default     = {}
}

variable "target_account_ids" {
  type        = list(string)
  description = "Set of AWS Account identifiers to distribute the AMI"
  default     = []
}

variable "terminate_instance_on_failure" {
  type        = bool
  description = "Enable if the build instance should be terminated when the pipeline fails"
  default     = true
}

variable "user_data_base64" {
  type        = string
  description = "Base64 encoded user data. Use this to provide commands or a command script to run when you launch your build instance"
  default     = null
}

variable "vpc_id" {
  type        = string
  description = "VPC ID to deploy the build instance into"
}

variable "working_directory" {
  type        = string
  description = "The working directory to be used during build and test workflows"
  default     = null
}
