# tf-aws-ec2-image-builder

EC2 AMI image builder.

## Usage
Simple example to add the CloudWatch agent to an Amazon provided AMI that runs on a schedule. Creates the encrypted AMI in the same account as the pipeline and adds permission for another:

```
module "eks_node-1-19" {
  source = "/path/to/module"

  name        = "amazon-eks-node-1-19"
  description = "EKS Kubernetes Worker AMI with AmazonLinux2 image"
  key_name    = "my-ssh-key"
  vpc_id      = "vpc-xxx"
  subnet_id   = "subnet-xxx"

  recipe_version = "1.0.0"
  parent_image   = "ami-0cb7121d3e6cf1afc"

  block_device_mappings = [
    {
      device_name = "/dev/xvda"
      ebs = {
        encrypted  = true
        kms_key_id = "arn:aws:kms:xxx"
      }
    }
  ]

  launch_permission = {
    user_ids = ["000000000002"]
  }

  components = [
    {
      component_arn = "arn:aws:imagebuilder:eu-west-1:aws:component/amazon-cloudwatch-agent-linux/1.0.1/1"
    }
  ]

  schedule = {
    schedule_expression = "cron(0 5 ? * TUE *)"
  }
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
#### Requirements

No requirements.

#### Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

#### Modules

No modules.

#### Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.ec2_image_builder](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_iam_instance_profile.ec2_image_builder](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.ec2_image_builder](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.amazon_ssm_managed_instance_core](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.ec2_instance_profile_for_image_builder](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.ec2_instance_profile_for_image_builder_ecr_container_builds](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_imagebuilder_distribution_configuration.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/imagebuilder_distribution_configuration) | resource |
| [aws_imagebuilder_image_pipeline.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/imagebuilder_image_pipeline) | resource |
| [aws_imagebuilder_image_recipe.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/imagebuilder_image_recipe) | resource |
| [aws_imagebuilder_infrastructure_configuration.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/imagebuilder_infrastructure_configuration) | resource |
| [aws_security_group.ec2_image_builder](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.all_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_security_group_rule.ssh_ingress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_iam_policy_document.ec2_assume](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

#### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_description"></a> [ami\_description](#input\_ami\_description) | Description to apply to the distributed AMI | `string` | `null` | no |
| <a name="input_ami_name"></a> [ami\_name](#input\_ami\_name) | Name of the created AMI | `string` | `null` | no |
| <a name="input_ami_tags"></a> [ami\_tags](#input\_ami\_tags) | Key-value map of tags to apply to the distributed AMI | `map(string)` | `{}` | no |
| <a name="input_block_device_mappings"></a> [block\_device\_mappings](#input\_block\_device\_mappings) | Configuration block(s) with block device mappings for the image recipe | `list(any)` | `[]` | no |
| <a name="input_components"></a> [components](#input\_components) | Map of components for the recipie | `list(any)` | n/a | yes |
| <a name="input_description"></a> [description](#input\_description) | Description to assign created resources | `string` | `null` | no |
| <a name="input_enhanced_image_metadata_enabled"></a> [enhanced\_image\_metadata\_enabled](#input\_enhanced\_image\_metadata\_enabled) | Whether additional information about the image being created is collected | `bool` | `true` | no |
| <a name="input_image_tests_configuration"></a> [image\_tests\_configuration](#input\_image\_tests\_configuration) | Configuration block with image tests configuration | `map(any)` | `null` | no |
| <a name="input_instance_metadata_options"></a> [instance\_metadata\_options](#input\_instance\_metadata\_options) | Configuration block with instance metadata options for the HTTP requests that pipeline builds use to launch EC2 build and test instances | `map(any)` | `null` | no |
| <a name="input_instance_types"></a> [instance\_types](#input\_instance\_types) | Set of EC2 Instance Types to use for the image build | `list(string)` | <pre>[<br>  "t2.nano",<br>  "t3.micro"<br>]</pre> | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | Name of EC2 Key Pair for the build instance | `string` | `null` | no |
| <a name="input_kms_key_id"></a> [kms\_key\_id](#input\_kms\_key\_id) | Amazon Resource Name (ARN) of the Key Management Service (KMS) Key to encrypt the distributed AMI | `string` | `null` | no |
| <a name="input_launch_permission"></a> [launch\_permission](#input\_launch\_permission) | Configuration block of EC2 launch permissions to apply to the distributed AMI | `map(any)` | `null` | no |
| <a name="input_launch_template_configuration"></a> [launch\_template\_configuration](#input\_launch\_template\_configuration) | Set of launch template configuration settings that apply to image distribution | `map(any)` | `null` | no |
| <a name="input_license_configuration_arns"></a> [license\_configuration\_arns](#input\_license\_configuration\_arns) | Set of Amazon Resource Names (ARNs) of License Manager License Configurations | `list(string)` | `[]` | no |
| <a name="input_log_retention"></a> [log\_retention](#input\_log\_retention) | CloudWatch logs retention in days | `string` | `30` | no |
| <a name="input_logging"></a> [logging](#input\_logging) | Configuration block with logging settings | `map(any)` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Name to assign created resources | `string` | n/a | yes |
| <a name="input_parent_image"></a> [parent\_image](#input\_parent\_image) | Parent (source) AMI | `string` | n/a | yes |
| <a name="input_recipe_version"></a> [recipe\_version](#input\_recipe\_version) | Version of the image recipe | `string` | `"1.0.0"` | no |
| <a name="input_resource_tags"></a> [resource\_tags](#input\_resource\_tags) | Key-value map of resource tags to assign to infrastructure created by the configuration | `map(string)` | `{}` | no |
| <a name="input_schedule"></a> [schedule](#input\_schedule) | Configuration block with schedule settings | `map(any)` | `null` | no |
| <a name="input_sns_topic_arn"></a> [sns\_topic\_arn](#input\_sns\_topic\_arn) | Amazon Resource Name (ARN) of SNS Topic for build notifications and alerts | `string` | `null` | no |
| <a name="input_ssh_ingress_cidr_blocks"></a> [ssh\_ingress\_cidr\_blocks](#input\_ssh\_ingress\_cidr\_blocks) | Set of CIDR blocks for SSH ingress | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_status"></a> [status](#input\_status) | Status of the image pipeline | `string` | `"ENABLED"` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | EC2 Subnet identifier for the build instance | `string` | n/a | yes |
| <a name="input_systems_manager_agent"></a> [systems\_manager\_agent](#input\_systems\_manager\_agent) | Configuration block for the Systems Manager Agent installed by default by Image Builder | `map(any)` | `null` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Key-value map of tags to apply to resources | `map(string)` | `{}` | no |
| <a name="input_target_account_ids"></a> [target\_account\_ids](#input\_target\_account\_ids) | Set of AWS Account identifiers to distribute the AMI | `list(string)` | `[]` | no |
| <a name="input_terminate_instance_on_failure"></a> [terminate\_instance\_on\_failure](#input\_terminate\_instance\_on\_failure) | Enable if the build instance should be terminated when the pipeline fails | `bool` | `true` | no |
| <a name="input_user_data_base64"></a> [user\_data\_base64](#input\_user\_data\_base64) | Base64 encoded user data. Use this to provide commands or a command script to run when you launch your build instance | `string` | `null` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | VPC ID to deploy the build instance into | `string` | n/a | yes |
| <a name="input_working_directory"></a> [working\_directory](#input\_working\_directory) | The working directory to be used during build and test workflows | `string` | `null` | no |

#### Outputs

| Name | Description |
|------|-------------|
| <a name="output_iam_role_name"></a> [iam\_role\_name](#output\_iam\_role\_name) | IAM role used by the build instance |
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | Security group used by the build instance |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
