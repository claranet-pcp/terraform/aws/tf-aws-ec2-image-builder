output "iam_role_name" {
  description = "IAM role used by the build instance"
  value       = aws_iam_role.ec2_image_builder.name
}

output "security_group_id" {
  description = "Security group used by the build instance"
  value       = aws_security_group.ec2_image_builder.id
}
