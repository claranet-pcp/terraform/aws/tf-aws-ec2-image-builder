locals {
  tags = merge(
    {
      Name = var.name
    },
    var.tags
  )
}

resource "aws_imagebuilder_component" "shell" {
  name     = var.name
  platform = var.platform
  version  = var.component_version

  data = yamlencode({
    phases = [{
      name = "build"
      steps = [{
        action         = var.action
        timeoutSeconds = var.timeout
        inputs = {
          commands = var.commands
        }
        name      = var.name
        onFailure = var.on_failure
      }]
    }]
    schemaVersion = 1.0
  })

  description = var.description
  tags        = local.tags
}
