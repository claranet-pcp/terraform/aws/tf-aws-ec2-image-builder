# shell
Simple shell component

## Usage
Simple example to only apply security updates:

```
module "update_security" {
  source = "/path/to/module"

  name     = "update-security"
  commands = ["yum update --security -y"]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_imagebuilder_component.shell](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/imagebuilder_component) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_action"></a> [action](#input\_action) | Action to perform e.g. ExecuteBash/ExecutePowerShell | `string` | `"ExecuteBash"` | no |
| <a name="input_commands"></a> [commands](#input\_commands) | List of commands to run | `list(string)` | n/a | yes |
| <a name="input_component_version"></a> [component\_version](#input\_component\_version) | Version of the component | `string` | `"1.0.0"` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of the component | `string` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the component | `string` | n/a | yes |
| <a name="input_on_failure"></a> [on\_failure](#input\_on\_failure) | Specifies what the step should do in case of failure (Abort\|Continue\|Ignore) | `string` | `"Abort"` | no |
| <a name="input_platform"></a> [platform](#input\_platform) | Platform for the component e.g. Linux/Windows | `string` | `"Linux"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Key-value map of tags to apply to resources | `map(string)` | `{}` | no |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Timeout of step in seconds | `number` | `300` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_arn"></a> [arn](#output\_arn) | Amazon Resource Name (ARN) of the component |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
