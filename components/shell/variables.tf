variable "action" {
  type        = string
  description = "Action to perform e.g. ExecuteBash/ExecutePowerShell"
  default     = "ExecuteBash"
}

variable "commands" {
  type        = list(string)
  description = "List of commands to run"
}

variable "description" {
  type        = string
  description = "Description of the component"
  default     = null
}

variable "name" {
  type        = string
  description = "Name of the component"
}

variable "on_failure" {
  type        = string
  description = "Specifies what the step should do in case of failure (Abort|Continue|Ignore)"
  default     = "Abort"
}

variable "platform" {
  type        = string
  description = "Platform for the component e.g. Linux/Windows"
  default     = "Linux"
}

variable "tags" {
  type        = map(string)
  description = "Key-value map of tags to apply to resources"
  default     = {}
}

variable "timeout" {
  type        = number
  description = "Timeout of step in seconds"
  default     = 300
}

variable "component_version" {
  type        = string
  description = "Version of the component"
  default     = "1.0.0"
}
