output "arn" {
  description = "Amazon Resource Name (ARN) of the component"
  value       = aws_imagebuilder_component.shell.arn
}
